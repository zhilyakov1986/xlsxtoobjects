﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;

namespace XLSXprocessing
{
    public class VADTO
    {
        public string DEAHIN1 { get; set; }


        public string DEAHIN2 { get; set; }


        public string Visn { get; set; }


        public string Class { get; set; }


        public string Station { get; set; }


        public string FacilityName { get; set; }


        public string Address1 { get; set; }


        public string Address2 { get; set; }


        public string City { get; set; }


        public string State { get; set; }


        public string Zip { get; set; }


        public string VaAffiliate { get; set; }

        public string Option { get; set; }
        public string ModEffectiveDate { get; set; }
        public string IhsRegion { get; set; }
    }

    internal class Program
    {

        private static OleDbConnection OpenConnection(string path)
        {
            OleDbConnection oledbConn = null;
            try
            {
                if (Path.GetExtension(path) == ".xls")
                {
                    oledbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + path +

                                                    "; Extended Properties= \"Excel 8.0;HDR=Yes;IMEX=2\"");
                }
                else if (Path.GetExtension(path) == ".xlsx")
                {
                    oledbConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" +

                                                    path + "; Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';");
                }

                oledbConn.Open();
            }
            catch (Exception ex)
            {
                //Error
            }
            return oledbConn;
        }

        private static IList<VADTO> ExtractEmployeeExcel(OleDbConnection oledbConn)
        {
            OleDbCommand cmd = new OleDbCommand(); ;
            OleDbDataAdapter oleda = new OleDbDataAdapter();
            DataSet dsVAInfo = new DataSet();

            cmd.Connection = oledbConn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM [ppvEligibilityListing$]"; //Excel Sheet Name ( Employee )
            oleda = new OleDbDataAdapter(cmd);
            oleda.Fill(dsVAInfo, "ppvEligibilityListing$");


            var dsVAInfoList = dsVAInfo.Tables[0].AsEnumerable().Select(s => new VADTO
            {

                DEAHIN1 = Convert.ToString(s[0] != DBNull.Value ? s[0] : ""),
                DEAHIN2 = Convert.ToString(s[1] != DBNull.Value ? s[1] : ""),
                Visn = Convert.ToString(s[2] != DBNull.Value ? s[2] : ""),
                Class = Convert.ToString(s[3] != DBNull.Value ? s[3] : ""),
                Station = Convert.ToString(s[4] != DBNull.Value ? s[4] : ""),
                FacilityName = Convert.ToString(s[5] != DBNull.Value ? s[5] : ""),
                Address1 = Convert.ToString(s[6] != DBNull.Value ? s[6] : ""),
                Address2 = Convert.ToString(s[7] != DBNull.Value ? s[7] : ""),
                City = Convert.ToString(s[8] != DBNull.Value ? s[8] : ""),
                State = Convert.ToString(s[9] != DBNull.Value ? s[9] : ""),
                Zip = Convert.ToString(s[10] != DBNull.Value ? s[10] : ""),
                VaAffiliate = Convert.ToString(s[11] != DBNull.Value ? s[11] : ""),
                Option = Convert.ToString(s[12] != DBNull.Value ? s[12] : ""),
                ModEffectiveDate = Convert.ToString(s[13] != DBNull.Value ? s[13] : ""),
                IhsRegion = Convert.ToString(s[14] != DBNull.Value ? s[14] : ""),

            }).ToList();

            return dsVAInfoList;
        }
        public static IList<VADTO> ReadExcel(string path)
        {
            IList<VADTO> objEmployeeInfo = new List<VADTO>();
            try
            {
                OleDbConnection oledbConn = OpenConnection(path);
                if (oledbConn.State == ConnectionState.Open)
                {
                    objEmployeeInfo = ExtractEmployeeExcel(oledbConn);
                    oledbConn.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return objEmployeeInfo;
        }

        private static void Main(string[] args)
        {
            string path = @"c:\Test\ppvEligibilityListing.xls";
            IList<VADTO> objExcelCon = ReadExcel(path);



            //using (FileStream fileStream = new FileStream(@"c:\Test\test.xlsx", FileMode.Open))
            //{
            //    ExcelPackage excelFile = new ExcelPackage(fileStream);
            //    var workSheet = excelFile.Workbook.Worksheets[1];

            //    IEnumerable<ListItemsDTO> listOfRecords =
            //        workSheet.ConvertSheetToObjects<ListItemsDTO>();
            //    var numberOfRecordsTotal = listOfRecords.Any() ? listOfRecords.Count() : 0;
            //    var numberOfRecordsAccepted = 0;
            //    // save that list in sql
            //    foreach (var record in listOfRecords)
            //    {
            //        Console.WriteLine(record);
            //    }
            //}

        }
    }
}




