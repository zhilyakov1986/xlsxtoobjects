﻿using System;
using XLSXprocessing.Attributes;

namespace XLSXprocessing.DTO
{
   
    public class VADTO
    {

        
        public string DEAHIN1 { get; set; }

        
        public string DEAHIN2 { get; set; }

        
        public string Visn { get; set; }

        
        public string Class { get; set; }

        
        public string Station { get; set; }

        
        public string FacilityName { get; set; }

        
        public string Address1 { get; set; }

        
        public string Address2 { get; set; }

        
        public string City { get; set; }

        
        public string State { get; set; }

        
        public string Zip { get; set; }

        
        public string VaAffiliate { get; set; }

    }
}