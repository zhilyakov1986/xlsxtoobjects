﻿using System;
using XLSXprocessing.Attributes;

namespace XLSXprocessing.DTO
{
    [Serializable]
    public class ListItemsDTO
    {

        [Column(1)]
        public string HealthcareSettingType { get; set; }

        [Column(2)]
        public string HealthcareSettingName { get; set; }

        [Column(3)]
        public string Address1 { get; set; }

        [Column(4)]
        public string Address2 { get; set; }

        [Column(5)]
        public string City { get; set; }

        [Column(6)]
        public string State { get; set; }

        [Column(7)]
        public string Zip { get; set; }

        [Column(8)]
        public string DEA { get; set; }

    }
}